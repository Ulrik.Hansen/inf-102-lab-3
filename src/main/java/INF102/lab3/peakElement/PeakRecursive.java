package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        return auxpeakElement(numbers, 0);
    }

    public int auxpeakElement(List<Integer> numbers, Integer index) {
        Boolean left;
        Boolean right;
        Integer cur = numbers.get(index);
        left = index == 0;
        if (!left) {
            left = numbers.get(index-1) <= cur;
        }
        right = index == numbers.size()-1;
        if (!right) {
            right = cur >= numbers.get(index+1);
        }
        if (right && left) {
            return cur;
        }
        return auxpeakElement(numbers, index+1);
        
    }

}
