package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        return sumaux(list,0);
    }

    public long sumaux(List<Long> list,Integer index) {
        if (index == list.size()) {return 0;}
        return list.get(index) + sumaux(list, index+1);
    }
    

}
