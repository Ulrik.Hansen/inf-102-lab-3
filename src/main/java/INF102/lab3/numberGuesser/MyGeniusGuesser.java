package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

    private int lowerbound;
	private int upperbound;
    private int step;


	@Override
    public int findNumber(RandomNumber number) {
    	lowerbound = number.getLowerbound();
		upperbound = number.getUpperbound();

        step = 0;

        Integer range;
        Integer guess;
		Integer middle;
		while (true) {

            step++;
            range = upperbound-lowerbound;
            middle = lowerbound + (range/ 2);
            guess = number.guess(middle);
            switch (guess) {
                case 0: return middle;
                case 1: {
                    upperbound = middle-1;
                    break;
                }
                case -1: {
                    lowerbound = middle+1;
                    break;
                }
            }
            

        }
	}

}
